package Tests;

import Pages.ReportsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

@SuppressWarnings("ALL")

public class Reportstests {

    ReportsPage rp = new ReportsPage();

    @And("^I enter designator as \"([^\"]*)\"$")
    public void iEnterDesignatorAs(String arg0) throws Throwable
        {
        rp.enterDesignator(arg0);
        }


    @And("^I choose version as \"([^\"]*)\"$")
    public void iChooseVersionAs(String arg0) throws Throwable
        {
        rp.pickASchedule(arg0);
        }

    @Then("^I should see the report confirmation \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void iShouldSeeTheReportConfirmation(String arg0, String arg1, String arg2) throws Throwable
        {
        rp.confirmreportGenerationMessage(arg0, arg1, arg2);
        }

    @And("^I choose service type as \"([^\"]*)\"$")
    public void iChooseServiceTypeAs(String arg0) throws Throwable
        {
        rp.pickAServiceType(arg0);
        }

    @And("^I enter service from as \"([^\"]*)\" and service to as \"([^\"]*)\"$")
    public void iEnterServiceFromAsAndServiceToAs(String arg0, String arg1) throws Throwable
        {
        rp.enterFromServiceNo(arg0);
        rp.enterToServiceNo(arg1);
        }

    @And("^I tick the \"([^\"]*)\" checkbox$")
    public void iTickTheCheckbox(String arg0) throws Throwable
        {
        rp.clickCheckBoxForAllNames();
        }


    @Then("^I should see the report confirmation \"([^\"]*)\"$")
    public void iShouldSeeTheReportConfirmation(String arg0) throws Throwable
        {
        rp.confirmreportGenerationMessage(arg0);
        }
}

package Tests;

import Pages.SchedulesPage;
import cucumber.api.java.en.Then;

@SuppressWarnings("ALL")
public class SchedulesTests {

    SchedulesPage sp = new SchedulesPage();


    @Then("^I should be able to add a new RSD level for \"([^\"]*)\" with \"([^\"]*)\" with \"([^\"]*)\" RSD$")
    public void iShouldBeAbleToAddANewRSDLevelForWithWithRSD(String arg0, String arg1, String arg2) throws Throwable
        {
        sp.addNewLevel();
        sp.verifyAddedRsdLevel(arg0, arg1, arg2);
        }
}

package Tests;

import Pages.BulkUpdatesPage;
import Pages.IteneraryPage;
import Pages.ReportsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@SuppressWarnings("ALL")
public class BulkUpdateTests {

    BulkUpdatesPage bp = new BulkUpdatesPage();
    ReportsPage rp = new ReportsPage();
    IteneraryPage ip = new IteneraryPage();

    @When("^I add designator as \"([^\"]*)\"$")
    public void iAddDesignatorAs(String arg0) throws Throwable
        {
        bp.inputDesignator(arg0);
        }

    @And("^I choose \"([^\"]*)\" as \"([^\"]*)\"$")
    public void iChooseAs(String arg0, String arg1) throws Throwable
        {
        if (arg0.equalsIgnoreCase("version"))
            {
            bp.chooseSchedulesVersion(arg1);
            } else if (arg0.equalsIgnoreCase("rsddate"))
            {
            bp.inputRsddate(arg1);
            } else if (arg0.equalsIgnoreCase("service type"))
            {
            bp.selectServiceType(arg1);
            } else if (arg0.equalsIgnoreCase("port"))
            {
            bp.selectPort(arg1);
            } else if (arg0.equalsIgnoreCase("name"))
            {
            rp.chooseCarrierName(arg1);
            } else if (arg0.equalsIgnoreCase("carrier version"))
            {
            rp.chooseVersion(arg1);
            } else if (arg0.equalsIgnoreCase("nac inc"))
            {
            rp.chooseNacInc(arg1);
            }
        }

    @When("^I click the \"([^\"]*)\" button$")
    public void iClickTheButton(String arg0) throws Throwable
        {
        if (arg0.equalsIgnoreCase("submit"))
            {
            bp.clickSumbit();
            Thread.sleep(3000);
            } else if (arg0.equalsIgnoreCase("edit"))
            {
            ip.clickOnEdit();
            } else if (arg0.equalsIgnoreCase("save"))
            {
            ip.clickSave();
            }
        }

    @Then("^I should see the status \"([^\"]*)\"$")
    public void iShouldSeeTheStatus(String arg0) throws Throwable
        {
        bp.verifyStatusonClick(arg0);
        }


    @And("^I should be able to remove schedules for \"([^\"]*)\" with \"([^\"]*)\"$")
    public void iShouldBeAbleToRemoveSchedulesForWith(String arg0, String arg1) throws Throwable
        {
        bp.selectACarrier(arg0, arg1);
        bp.verifyNoSchedulesdataPresent();
        }

    /*Copy schedules Tests*/

    @When("^I enter the \"([^\"]*)\" as \"([^\"]*)\" with \"([^\"]*)\"$")
    public void iEnterTheAsWith(String arg0, String arg1, String arg2) throws Throwable
        {
        if (arg0.equalsIgnoreCase("from carrier"))
            {
            bp.enterFromCarrier(arg1);
            bp.chooseFromversion(arg2);
            } else if (arg0.equalsIgnoreCase("to carrier"))
            {
            bp.enterTocarrier(arg1);
            bp.chooseToVersion(arg2);
            }
        }

    @Then("^the confirmation should contain \"([^\"]*)\"$")
    public void theConfirmationShouldContain(String arg0) throws Throwable
        {
        bp.confirmationOfCopy(arg0);
        }

    //Verify all copied schedules to the carrier
    @Then("^\"([^\"]*)\" schedules should be copied \"([^\"]*)\" with \"([^\"]*)\" version to the \"([^\"]*)\" with \"([^\"]*)\" version$")
    public void theSchedulesShouldBeCopiedWithToTheWith(String arg0, String arg1, String arg2, String arg3, String arg4) throws Throwable
        {
        bp.verifyCopiedSchedules(arg0, arg1, arg2, arg3, arg4);
        }


    @And("^I enter \"([^\"]*)\" as \"([^\"]*)\"$")
    public void iEnterAs(String arg0, String arg1) throws Throwable
        {
        switch (arg0)
            {
            case "from date":
                bp.enterFromDateRange(arg1);
                break;

            case "to date":
                bp.enterToDateRange(arg1);
                break;
            case "from service number":
                bp.enterFromServiceNumber(arg1);
                break;

            case "to service number":
                bp.enterToServiceNumber(arg1);
                break;
            case "email":
                rp.enterEmail(arg1);
                break;
            case "carrier code":
                rp.enterCarrierCode(arg1);
                break;

            case "file name":
                bp.enterFileName(arg1);
                break;

            }
        }

    /*Test to verify copied schedule sby date and service range*/

    @Then("^\"([^\"]*)\" schedules should be copied  to the \"([^\"]*)\" with \"([^\"]*)\" version between \"([^\"]*)\" and \"([^\"]*)\"$")
    public void schedulesShouldBeCopiedToTheWithVersionBetweenAnd(String arg0, String arg1, String arg2, String arg3, String arg4) throws Throwable
        {
        switch (arg0)
            {
            case "service range":
                bp.verifyCopiedServiceByRange(arg1, arg2, arg3, arg4);
                break;

            case "date range":
                bp.verifyCopiedServiceByDate(arg1, arg2, arg3, arg4);
                break;
            }
        }

    /*Test for verify by service type*/

    @Then("^schedules should be copied  to the \"([^\"]*)\" with \"([^\"]*)\" version with service type \"([^\"]*)\"$")
    public void schedulesShouldBeCopiedToTheWithVersionWithServiceType(String arg1, String arg2, String arg3) throws Throwable
        {
        bp.verifyCopiedServiceByType(arg1, arg2, arg3);
        }

    /*Test for verify by port */
    @Then("^schedules should be copied  to the \"([^\"]*)\" with \"([^\"]*)\" version with port \"([^\"]*)\"$")
    public void schedulesShouldBeCopiedToTheWithVersionWithPort(String arg0, String arg1, String arg2) throws Throwable
        {
        bp.verifyCopiedServiceByPort(arg0, arg1, arg2);
        }


    @And("^the carrier \"([^\"]*)\" with version \"([^\"]*)\" should be updated with equipment \"([^\"]*)\"$")
    public void theCarrierWithVersionShouldBeUpdatedWithEquipment(String arg0, String arg1, String arg2) throws Throwable
        {
        bp.verifyEquipmentByAdhocBulkUpdate(arg0, arg1, arg2);
        }
}

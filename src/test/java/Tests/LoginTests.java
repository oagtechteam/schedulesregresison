package Tests;

import Pages.LoginPage;
import cucumber.api.java.en.Given;

@SuppressWarnings("ALL")
public class LoginTests {

    LoginPage loginPage = new LoginPage();

    @Given("^Iam logged in$")
    public void iamLoggedIn() throws Throwable
        {
        loginPage.loginIfUserisNotLoggedIn();
        }
}

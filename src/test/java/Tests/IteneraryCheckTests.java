package Tests;

import Pages.IteneraryPage;
import Pages.SchedulesPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@SuppressWarnings("ALL")
public class IteneraryCheckTests {

    SchedulesPage sp = new SchedulesPage();
    IteneraryPage ip = new IteneraryPage();


    @And("^I  edit and  save each itenerary$")
    public void iEditAndSaveEachItenerary() throws Throwable
        {
        ip.editAndSaveItenerary();
        }

    @Then("^there should be no errors$")
    public void thereShouldBeNoErrors() throws Throwable
        {
        System.out.println("NO ERRORS PRESENT");
        }


    @When("^I search for \"([^\"]*)\" with \"([^\"]*)\" with \"([^\"]*)\" RSD$")
    public void iSearchForWithWithRSD(String arg0, String arg1, String arg2) throws Throwable
        {
        sp.chooseACarrierWithversion(arg0, arg1, arg2);
        }


    @And("^I choose (\\d+) st flight and  itenerary$")
    public void iChooseStFlightAndItenerary(int arg0) throws Throwable
        {
        ip.chooseFirstFlightAndItenerary(arg0);
        }

    @When("^I delete the eff from and eff to date$")
    public void iDeleteTheEffFromAndEffToDate() throws Throwable
        {
        ip.deleteEffEndDate();
        ip.deleteEffEndDate();
        }

    @Then("^the date should be added automatically$")
    public void theDateShouldBeAddedAutomatically() throws Throwable
        {
        ip.verifyEffDatesAutofill();
        }

    @When("^I delete the \"([^\"]*)\" field$")
    public void iDeleteTheField(String arg0) throws Throwable
        {
        switch (arg0.toLowerCase())
            {
            case "operational days":
                ip.deleteDaysOfOperation();
                break;
            case "dep port":
                ip.deleteDepPort();
                break;

            case "arr port":
                ip.deleteArrPort();
                break;

            }

        }

    @When("^I add more then seven characters in operational days field$")
    public void iAddMoreThenSevenCharactersInOperationalDaysField() throws Throwable
        {
        ip.addMoreThanSevenDaysInDaysOfOPFiled();
        }


    @Then("^the error message \"([^\"]*)\" should be shown$")
    public void theErrorMessageShouldBeShown(String arg0) throws Throwable
        {
        ip.iteneraryErrorMeesages(arg0);
        }

    @And("^the \"([^\"]*)\" error message \"([^\"]*)\" should be displayed$")
    public void theErrorMessageShouldBeDisplayed(String arg0, String arg1) throws Throwable
        {
        ip.verifyFieldmessagesOnErrors(arg0, arg1);
        }


}

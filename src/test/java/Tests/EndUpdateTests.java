package Tests;

import Pages.LogWorkDetail;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;

public class EndUpdateTests {

    LogWorkDetail lw = new LogWorkDetail();

    @Then("^I should be able to end update for \"([^\"]*)\" carrier$")
    public void iShouldBeAbleToEndUpdateForACarrier(String arg0) throws Throwable
        {
        lw.clearCarrierUpdate(arg0);
        }
}

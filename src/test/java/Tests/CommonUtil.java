package Tests;

import Pages.*;
import Utils.WebConnector;
//import cucumber.api.Result;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

@SuppressWarnings("ALL")
public class CommonUtil {

    BulkUpdatesPage bp = new BulkUpdatesPage();
    TopBarPage tp = new TopBarPage();
    LoginPage lp = new LoginPage();
    LeftHandMenuPage lhp = new LeftHandMenuPage();
    IteneraryPage ip = new IteneraryPage();

    //Take a screenshot if the Scenario is failed!

    @After()
    public void tearDown(Scenario scenario) throws InterruptedException
        {
//          if (scenario.getStatus()==Result.Type.FAILED)
        if (scenario.isFailed())
            {
            scenario.write("Failed Scenario: " + scenario.getName());
            //             Take a screenshot
            final byte[] screenshot = ((TakesScreenshot) WebConnector.currentDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
            }

        }

    /* remove schedules before copying*/
    @Before()
    public void tearDownScenario(Scenario scenario) throws InterruptedException
        {

        if (scenario.getName().toLowerCase().contains("copy schedules"))
            {
            lp.loginIfUserisNotLoggedIn();
            tp.switchbacfromFrame();
            tp.switchToTopBar();
            tp.clickSchedules();
            lhp.clickOnAMenu("Bulk Updates");
            lhp.clickOnAMenu("Remove Schedules");
            bp.inputDesignator("zz");
            bp.chooseSchedulesVersion("Test");
            bp.clickSumbit();
            Thread.sleep(2000);
            bp.verifyStatusonClick("Remove Schedules is successful");
            Thread.sleep(2000);
            }
        }

    @After()
    public void tearDownAdhoc(Scenario scenario) throws InterruptedException
        {
        if (scenario.getName().toLowerCase().contains("ad hoc"))
            {
            lp.loginIfUserisNotLoggedIn();
            tp.switchbacfromFrame();
            tp.switchToTopBar();
            tp.clickSchedules();
            lhp.clickOnAMenu("Bulk Updates");
            lhp.clickOnAMenu("Ad-hoc bulk update");
            bp.inputDesignator("SD");
            bp.chooseSchedulesVersion("Training 2");
            bp.enterFileName("prevalidate.txt");
            bp.clickSumbit();
            Thread.sleep(2000);
            bp.verifyStatusonClick("Adhoc bulk update is successful");
            Thread.sleep(2000);
            new UpmPage().initiateCommit("SD", "Training 2", "BulkUpdate");
            }

        }
}

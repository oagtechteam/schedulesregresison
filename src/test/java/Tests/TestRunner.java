package Tests;


import Pages.BasePage;
import Pages.LogWorkDetail;
import Utils.WebConnector;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@SuppressWarnings("ALL")
@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty", "html:target/cucumber-html-report",
        "json:target/cucumber.json"},
        features = "src/test/resources",
        tags = {"@reports"}, monochrome = true)


public class TestRunner {



    @BeforeClass

    public static void endUpdate() throws InterruptedException
        {

        LogWorkDetail lw = new LogWorkDetail();
        lw.clearCarrierUpdate("ZZ");
        }


    /* Closes the browser and all instances after all the test runs are completed or after executing test runner class.*/


    @AfterClass
    public static void quitBrowser()
        {
        WebConnector.quit();
        }


}

package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

@SuppressWarnings("ALL")
public class NtpPage extends BasePage {

    private By designator = By.name("designator");
    public static By rsddateField = By.name("rsd");
    public static By sumbitButton = By.xpath("//*[@src='/SchedulesWeb/images/buttons/submit.gif']");

    /* Resetting the commit date via UI */
    public void resetCommitDate(String carrier, String version, String date) throws InterruptedException
        {
        input(designator, carrier);
        selectByVisibleText(SchedulesPage.scheduleVersionDropdown, version);
        input(rsddateField, date);
        click(sumbitButton);
//        confirmAlert();
        Assert.assertEquals("Commit date reset is successful", getTextOfaElement(UpmPage.confirmation));
        Thread.sleep(5000);

        }
}

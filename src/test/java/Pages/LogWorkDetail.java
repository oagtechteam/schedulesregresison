package Pages;

import Utils.WebConnector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class LogWorkDetail extends BasePage {

    private By userIdField = By.name("userIdClaimed");
    private By carriers = By.xpath("//*[@id='dblclickPrint']/td[1]");
    private By endUpdateCheckbox = By.name("endUpdateProcess");
    private By mainCarrierDetailsTable = By.id("TableMainCarierDetails");
    private By filterButton = By.xpath("//*[@src='/SchedulesWeb/images/buttons/filter.gif']");
    public static By saveButton = By.xpath("//*[@src='/SchedulesWeb/images/buttons/save.gif']");
    private By updateEndField = By.xpath("//*[@id='dblclickPrint']/td[11]");

    LoginPage lp = new LoginPage();
    TopBarPage tp = new TopBarPage();

    public void clearCarrierUpdate(String carrier) throws InterruptedException
        {

        lp.loginIfUserisNotLoggedIn();
        tp.switchbacfromFrame();
        tp.switchToTopBar();
        tp.clickSchedules();
        new LeftHandMenuPage().clickOnAMenu("Update process Monitor");
        new LeftHandMenuPage().clickOnAMenu("Log Work Detail");
        waitForElementToBePresent(By.id("printTable"));
        findElement(By.id("printTable")).findElement(UpmPage.carrierCodeField).sendKeys(carrier);
        input(userIdField, "Auto");
        Thread.sleep(1000);
        click(filterButton);
//        waitFortextTobePresent(UpmPage.noOfItems, "1- 1 of 1");
        List<WebElement> carriersList = findElement(By.id("scrollArea")).findElements(carriers);
        List<WebElement> updateEndFields = findElement(By.id("scrollArea")).findElements(updateEndField);

        if (carriersList.size() > 0)
            {
            for (int i = 0; i < carriersList.size(); i++)
                {
                System.out.println("The carrier present in the log work detail table is*********  " + carriersList.get(i).getText());
                System.out.println("The carrier is " + carriersList.get(i).getText() + " with an update END time stamp of " + updateEndFields.get(i).getText());
                if ((carriersList.get(i).getText().equalsIgnoreCase(carrier)) && (updateEndFields.get(i).getText().isEmpty()))
                    {
                    Actions actions = new Actions(WebConnector.currentDriver());
                    actions.moveToElement(carriersList.get(i)).doubleClick().perform();
                    Thread.sleep(3000);
                    waitForElementToBePresent(mainCarrierDetailsTable);
                    if (isElementPresent(endUpdateCheckbox))
                        {
                        WebElement checkBox = findElement(mainCarrierDetailsTable).findElement(endUpdateCheckbox);
                        if (!checkBox.isSelected())
                            {
                            checkBox.click();
                            }

                        click(saveButton);
                        Thread.sleep(5000);
                        }
                    }
                }

            } else
            {
            System.out.println("There is no update in progress for the carrier" + carrier);
            }
        }
}

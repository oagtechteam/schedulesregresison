package Pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SuppressWarnings("ALL")
public class IteneraryPage extends BasePage {

    private By iteneraryResults = By.cssSelector(".txt1>a");
    public static By saveButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/save.gif']");
    private By cancelButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/cancel.gif']");
    private By editButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/edit.gif']");
    private By completeButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/complete.gif']");
    private By noOfItemsDropdown = By.cssSelector("select[name='noOfItems']");
    private By routeTimes = By.id("RouteTimes");
    private By serviceTypeField = By.xpath("//input[contains(@name,'serviceTypeCd')]");
    private By equipmentTypeField = By.xpath("//input[contains(@name,'equipmentTypeCd')]");
    private By effStartDate = By.name("effStartDate");
    private By effEndDate = By.name("effEndDate");
    private By daysOfOpField = By.id("operDaysOfWeek");
    private By depPort = By.xpath("//input[contains(@name,'depPortCd')]");
    private By arrPort = By.xpath("//input[contains(@name,'arrPortCd')]");


    /*Error message field on iutenerary*/
    private By errormessageFieldOnItenerary = By.name("clientErrors");
    private By minimumDaysError = By.id("dayMinLengthError");
    private By maximumDaysError = By.id("dayMaxLengthError");
    private By depPortError = By.xpath("//label[contains(@id,'depPortCdError')]");
    private By arrPortError = By.xpath("//label[contains(@id,'arrPortCdError')]");

    SchedulesPage sp = new SchedulesPage();

    /*  Edit and Save on each itenerary */
    public void editAndSaveItenerary() throws InterruptedException
        {

        Thread.sleep(5000);

        WebElement area = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> flights = area.findElements(SchedulesPage.flightResults);
        flights.get(0).click();
        Thread.sleep(2000);
        editEachItenarary();

        }


    /*        Click and edit each itenerary */

    public void editEachItenarary() throws InterruptedException
        {
        do
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);
            itenaries.get(0).click();
            clickOnEdit();
            clickSave();
            waitForElementToBePresent(By.linkText("Next service"));
            Thread.sleep(3000);
            findElement(By.linkText("Next service")).click();
            }
        while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."));


        waitForElementExplicitly(completeButton);

        isElementDisplayed(completeButton);

        click(completeButton);

        }

    /*        Verify service type field after copying schedules (with out editing the itenerary) */
    public void verifyServiceTypeFieldonEachitenerary(String serviceType) throws InterruptedException
        {

        Thread.sleep(5000);

        WebElement fligtsArea = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> flights = fligtsArea.findElements(SchedulesPage.flightResults);
        flights.get(0).click();
        Thread.sleep(2000);
        do
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);
            itenaries.get(0).click();

            do
                {
//                clickOnEdit();
                List<WebElement> types = findElement(routeTimes).findElements(serviceTypeField);
                System.out.println("The total number of service types fields found per itenerary are " + types.size());
                for (WebElement type : types)
                    {
                    System.out.println("The value of the service type field is**********[ " + type.getAttribute("value") + " ]");
                    Assert.assertTrue(type.getAttribute("value").equalsIgnoreCase(serviceType));
                    }
                findElement(By.linkText("Next Itinerary")).click();
                } while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of Itinerary."));
            click(cancelButton);


            waitForElementToBePresent(By.linkText("Next service"));
            Thread.sleep(3000);
            findElement(By.linkText("Next service")).click();
            }
        while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."));

        if (findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."))
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);
//            itenaries.get(0).click();
//            clickOnEdit();
//            clickSave();
            }

        waitForElementExplicitly(completeButton);

        isElementDisplayed(completeButton);

        click(completeButton);


        }

    /* Verify the effective from and To date son Itenerary */

    public void verifyFromAndToDatesonItenerary(String fromDate, String toDate) throws InterruptedException, ParseException
        {


        Thread.sleep(5000);

        WebElement fligtsArea = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> flights = fligtsArea.findElements(SchedulesPage.flightResults);
        flights.get(0).click();
        Thread.sleep(2000);
        do
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);
            itenaries.get(0).click();

            do
                {


                SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
                Date startDate = sdf.parse(findElement(effStartDate).getAttribute("value").toLowerCase());
                System.out.println("The eff from date on the itenerary is***** " + startDate);

                Date endDate = sdf.parse(findElement(effEndDate).getAttribute("value").toLowerCase());
                System.out.println("The eff To date on the itenerary is***** " + endDate)
                ;

                Date from = sdf.parse(fromDate);
                Date to = sdf.parse(toDate);

                Assert.assertTrue("start date is before the range", isDateAfterStartDate(startDate, from));
                Assert.assertTrue("End date is out of the range", isDateBeforeEndDate(endDate, to));

                findElement(By.linkText("Next Itinerary")).click();
                } while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of Itinerary."));
            click(cancelButton);


            waitForElementToBePresent(By.linkText("Next service"));
            Thread.sleep(3000);
            findElement(By.linkText("Next service")).click();
            }
        while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."));

        if (findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."))
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);

            }

        waitForElementExplicitly(completeButton);

        isElementDisplayed(completeButton);

        click(completeButton);


        }

    /*Verify copied schedules by port*/

    public void verifyPortFieldsContainCopiedPort(String port) throws InterruptedException
        {

        Thread.sleep(5000);

        WebElement fligtsArea = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> rows = fligtsArea.findElements(By.tagName("tr"));
        for (int i = 2; i < rows.size(); i++)
            {
            System.out.println("The text on the port field is****** " + rows.get(i).findElements(By.tagName("td")).get(1).getText());
            Assert.assertTrue("The service is not copie dby Port", rows.get(i).findElements(By.tagName("td")).get(1).getText().contains(port));
            }

        }


    /* Verify Bulk Update copied equipment type*/

    public void verifyEquipmentField(String equipmentNumber) throws InterruptedException
        {
        Thread.sleep(5000);

        WebElement fligtsArea = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> flights = fligtsArea.findElements(SchedulesPage.flightResults);
        flights.get(0).click();
        Thread.sleep(2000);
        do
            {
            WebElement area = findElement(SchedulesPage.searchResultsarea);
            List<WebElement> itenaries = area.findElements(iteneraryResults);
            itenaries.get(0).click();

            do
                {

                List<WebElement> types = findElement(routeTimes).findElements(equipmentTypeField);
                System.out.println("The total number of equipment types fields found per itenerary are " + types.size());
                for (WebElement type : types)
                    {
                    System.out.println("The value of the equipment type field is**********[ " + type.getAttribute("value") + " ]");
                    Assert.assertTrue(type.getAttribute("value").equalsIgnoreCase(equipmentNumber));
                    }
                findElement(By.linkText("Next Itinerary")).click();
                } while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of Itinerary."));
            click(cancelButton);


            waitForElementToBePresent(By.linkText("Next service"));
            Thread.sleep(3000);
            findElement(By.linkText("Next service")).click();
            }
        while (!findElement(By.id("ivIdError")).getText().equalsIgnoreCase("End of flights for carrier reached."));


        waitForElementExplicitly(completeButton);

        isElementDisplayed(completeButton);

        click(completeButton);


        }


    /*Choose first flight and first itenerary*/

    public void chooseFirstFlightAndItenerary(int flightAndItenerary) throws InterruptedException
        {

        sp.chooseFirstFlightFromResults(flightAndItenerary);
        chooseFirstIteneraryFromResults(flightAndItenerary);

//        WebElement area = findElement(SchedulesPage.searchResultsarea);
//        List<WebElement> itenaries = area.findElements(iteneraryResults);
//        itenaries.get(flightAndItenerary).click();


        }

    /*Verify the eff start and end date are autofilled after deleting them and saving itenerary*/
    public void verifyEffDatesAutofill() throws InterruptedException
        {

        chooseFirstIteneraryFromResults(1);
        clickOnEdit();
        System.out.println("The value of the eff start date is ***********" + getTextOfaElement(effStartDate));
        Assert.assertTrue(findElement(effStartDate).getAttribute("value").equals("00XXX00"));

        System.out.println("The value of the eff end date is ***********" + getTextOfaElement(effEndDate));
        Assert.assertTrue(findElement(effEndDate).getAttribute("value").equals("00XXX00"));
        }


    /*Delete eff start date*/
    public void deleteEffStartDate()
        {
        clearField(effStartDate);

        }

    /*Delete eff to dfate*/

    public void deleteEffEndDate()
        {
        clearField(effEndDate);
        }

    /*    Click on the edit button */
    public void clickOnEdit()
        {
        click(editButton);
        }

    /* click on the save button */
    public void clickSave()
        {
        click(saveButton);
        }

    /*Click on Route Sumamry */

    public void clickOnRouteSummary()
        {
        clickBreadCrumbsByName("route summary");

        }

    /*Choose first option from results*/
    public void chooseFirstIteneraryFromResults(int index) throws InterruptedException
        {

        Thread.sleep(2000);

        WebElement fligtsArea = findElement(SchedulesPage.searchResultsarea);
        List<WebElement> flights = fligtsArea.findElements(iteneraryResults);
        flights.get(index - 1).click();
        }

    /*Delete days of Op field*/
    public void deleteDaysOfOperation()
        {
        clearField(daysOfOpField);
        }
    /*Delete dep port field*/

    public void deleteDepPort()
        {
        clearField(depPort);
        }

    /*Delete arrival port*/
    public void deleteArrPort()
        {
        clearField(arrPort);
        }


    /*Ad dmore than 7 chars oin to operational days field*/

    public void addMoreThanSevenDaysInDaysOfOPFiled()
        {
        input(daysOfOpField, "123456789");
        }

    /*verify error message generate don itenerary Fields*/
    public void iteneraryErrorMeesages(String iteneraryErrorMessage)
        {
        waitForElementToBeVisible(errormessageFieldOnItenerary);
        Assert.assertEquals(iteneraryErrorMessage, getTextOfaElement(errormessageFieldOnItenerary));

        }
    /*Minimum days required Error message validation*/

    public void verifyFieldmessagesOnErrors(String type, String message)
        {
        switch (type.toLowerCase())
            {

            case "minimum days":
                Assert.assertEquals(message, getTextOfaElement(minimumDaysError));
                break;

            case "maximun days":
                Assert.assertEquals(message, getTextOfaElement(maximumDaysError));
                break;
            case "dep port":
                Assert.assertEquals(message, getTextOfaElement(depPortError));
                break;
            case "arr port":
                Assert.assertEquals(message, getTextOfaElement(arrPortError));
                break;


            }


        }


}

package Pages;

import Utils.WebConnector;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
@SuppressWarnings("ALL")
public class Buttons extends BasePage {

    private By saveButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/save.gif']");
    private By cancelButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/cancel.gif']");
    private By editButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/edit.gif']");
    private By completeButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/complete.gif']");
    private By signOutButton = By.cssSelector("img[src='images/buttons/signout.gif']");
    private By signInButton = By.xpath("//*[@src='images/buttons/signin.gif']");
    public static By filterButton = By.xpath("//*[@id='scrollArea']/table/tbody/tr[2]/td[6]/a/img");
    private By goButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/go.gif']");
    private By initiateCommitButton = By.xpath("//img[@title='Commit chosen Carriers']");
    public static By sumbitButton = By.xpath("//*[@src='/SchedulesWeb/images/buttons/submit.gif']");


    public void clickSaveButton()
        {
        click(saveButton);
        }

    public void clickCancelButton()
        {
        click(cancelButton);
        }

    public void clickEditButton()
        {
        click(editButton);
        }

    public void clickCompleteButton()
        {
        click(completeButton);
        }

    public void clickSigninButton()
        {
        click(signInButton);
        }

    public void clickSignOutButton()
        {
        click(signOutButton);
        }

    public void clickFilterButton()
        {
        click(filterButton);
        }

    public void clickGoButton()
        {
        click(goButton);
        }

    public void clickInitiateCommitButton()
        {

        click(initiateCommitButton);
        }

    public void clicksubmitButton()
        {

        click(sumbitButton);
        }

    public void waitForButtonToBeClickable(By locator)
        {
        WebDriverWait wait = new WebDriverWait(WebConnector.currentDriver(), 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
        }

}

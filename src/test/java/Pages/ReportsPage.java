package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

@SuppressWarnings("ALL")
public class ReportsPage extends BasePage {

    /* Listing report locators*/
    private By emailField = By.name("emailAddress");
    private By designatorField = By.id("CARRIER_TEXT_ID_0");
    private By schedulesVersionDropdown = By.cssSelector("select[name='scheduleVersion[0]']");
    private By serviceTypeDropdown = By.cssSelector("select[name='serviceType[0]']");
    private By serviceRangeFromField = By.name("flightNumberRange[0]");
    private By serviceRangeToField = By.name("flightNumberRange[1]");
    private By reportsConfirmation = By.className("ErrorLines");

    /* deleted carrier report locators*/
    private By carrierCodeField = By.name("carrierCode");
    private By carrierNameDropdown = By.cssSelector("select[name='carrierName']");
    private By versionDropdown = By.cssSelector("select[name='version']");
    private By searchForAllNamesCheckbox = By.name("searchForAllNames");

    /*SSM INC NAC reports locators*/
    private By nacIncDropdown = By.cssSelector("select[name='nacinc']");

    /* click Submit*/
    public void clickSumbit() throws InterruptedException
        {
        click(NtpPage.sumbitButton);


        }

    /* Enter email adress*/

    public void enterEmail(String email)
        {
        input(emailField, email);
        }


    /* Enter designator in field*/
    public void enterDesignator(String designator)
        {
        input(designatorField, designator);
        }

    /* pick a schedules version*/
    public void pickASchedule(String schedule) throws InterruptedException
        {
        selectByVisibleText(schedulesVersionDropdown, schedule);
        Thread.sleep(1000);
        }

    /* pick a service type*/
    public void pickAServiceType(String serType)
        {
        selectByVisibleText(serviceTypeDropdown, serType);
        }


    /* Enter  from servcie number  in field*/
    public void enterFromServiceNo(String serviceNo)
        {
        input(serviceRangeFromField, serviceNo);
        }

    /* Enter  to servcie number  in field*/
    public void enterToServiceNo(String serviceNo)
        {
        input(serviceRangeToField, serviceNo);
        }
    /* Confirm the confirmation of reports*/

    public void confirmreportGenerationMessage(String confirmation)
        {
        waitForElementToBeVisible(reportsConfirmation);
        Assert.assertEquals(confirmation, getTextOfaElement(reportsConfirmation));
        }

    public void confirmreportGenerationMessage(String confirmation1,String env,String conf)
        {
        waitForElementToBeVisible(reportsConfirmation);

        if (getCurrentUrl().contains("dv11"))
            {
            env="dev";
            Assert.assertEquals(confirmation1+env+conf, getTextOfaElement(reportsConfirmation));
            } else if (getCurrentUrl().contains("ut05"))
            {
            env="utc";
            Assert.assertEquals(confirmation1+env+conf, getTextOfaElement(reportsConfirmation));
            } else if (getCurrentUrl().contains("database"))
            {
            env="prd";
            Assert.assertEquals(confirmation1+env+conf, getTextOfaElement(reportsConfirmation));
            }


        }

    /* enter carrier code in deleted carrier report fields*/
    public void enterCarrierCode(String code)
        {
        input(carrierCodeField, code);
        }

    /* choose a carrier name*/
    public void chooseCarrierName(String name)
        {
        selectByVisibleText(carrierNameDropdown, name);
        }
    /* pick version from dropdown*/

    public void chooseVersion(String version)
        {
        selectByVisibleText(versionDropdown, version);
        }
    /* checkbox tock for search for all names*/

    public void clickCheckBoxForAllNames()
        {
        click(searchForAllNamesCheckbox);
        }

    /* Choose nac inc from dropdown*/
    public void chooseNacInc(String type)
        {
        selectByVisibleText(nacIncDropdown, type);

        }
}


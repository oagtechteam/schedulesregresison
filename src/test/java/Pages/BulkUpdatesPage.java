package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.util.List;

@SuppressWarnings("ALL")
public class BulkUpdatesPage extends BasePage {

    SchedulesPage sp = new SchedulesPage();
    TopBarPage tp = new TopBarPage();

    /* Remove Schedules locators*/
    private By designator = By.name("designator");
    private By carrierData = By.xpath("//*[@id='scrollArea']/table[2]/tbody/tr/td/font");

    /* Copy Schedules Locators*/
    private By fromCarrier = By.name("fromCarrier");
    private By toCarrier = By.name("toCarrier");
    private By fromVersion = By.cssSelector("select[name='fromSchedVersion']");
    private By toVersion = By.cssSelector("select[name='toSchedVersion']");
    private By fromdateRange = By.name("dateRangeFrom");
    private By toDateRange = By.name("dateRangeTo");
    private By fromSerNo = By.name("flightNumber[2]");
    private By toSerNo = By.name("flightNumber[3]");
    private By serviceTypeDropdown = By.cssSelector("select[name='serviceType']");
    private By portDropdown = By.cssSelector("select[name='airport']");

    /* Ad HOC BULK UPDATE locators*/
    private By fileNameField = By.name("adhocBulkUpdate");


    IteneraryPage ip = new IteneraryPage();

    /*   Remove schecdules function */
    public void inputDesignator(String des)
        {
        input(designator, des);

        }

    /* Choose a Schedule version on Remove schedules screen */
    public void chooseSchedulesVersion(String version) throws InterruptedException
        {
        sp.selectScheduleVersion(version);
        }

    /* click Submit */
    public void clickSumbit() throws InterruptedException
        {
        click(NtpPage.sumbitButton);
//        Thread.sleep(5000);
//        confirmAlert();

        }

    /* Input the rsd date in the date field */
    public void inputRsddate(String date)
        {
        input(NtpPage.rsddateField, date);

        }

    /* Verify the confirmation after clicking remove schedules */
    public void verifyStatusonClick(String msg) throws InterruptedException
        {
        Thread.sleep(3000);
//        waitForElementToBePresent(UpmPage.confirmation);
        Assert.assertEquals(msg, getTextOfaElement(UpmPage.confirmation));
        }


    /* Verification of the rmeoved schedules-No data found text */
    public void selectACarrier(String carrier, String version) throws InterruptedException
        {
        tp.switchbacfromFrame();
        tp.switchToTopBar();
        tp.clickSchedules();
        sp.carrierName(carrier);
        sp.selectScheduleVersion(version);
//        sp.selectRsdlevel(rsd);
        sp.clickFilterButton();
        sp.clickIfRsdCarrier();
        sp.isFooterPresent();
        Thread.sleep(3000);


        }

    /*       Confirm no data present for schedules */
    public void verifyNoSchedulesdataPresent()
        {
        Assert.assertEquals("No Data Found", getTextOfaElement(carrierData));
        }

    /*        Copy Schedules ==============================================================================================*/

    /*      Enter the From carrier to be copied from */

    public void enterFromCarrier(String carrier)
        {
        input(fromCarrier, carrier);
        }

    /*        Choose the From schedule version */
    public void chooseFromversion(String version)
        {
        selectByVisibleText(fromVersion, version);
        }

    /*        Enter the to carrier to be copied */
    public void enterTocarrier(String carrier)
        {
        input(toCarrier, carrier);
        }

    /*        Choose the the to schedule version  */
    public void chooseToVersion(String version)
        {
        selectByVisibleText(toVersion, version);
        }
    /* Verify the confirmation status without job id */

    public void confirmationOfCopy(String msg) throws InterruptedException
        {
//        waitForElementToBePresent(UpmPage.confirmation);
        Assert.assertTrue("Copy wasnt started due to the reason --" + getTextOfaElement(UpmPage.confirmation), getTextOfaElement(UpmPage.confirmation).contains(msg));
        if (getCurrentUrl().contains("dv11"))
            {
            Thread.sleep(75000);
            } else if (getCurrentUrl().contains("ut05"))
            {
            Thread.sleep(30000);
            } else if (getCurrentUrl().contains("database"))
            {
            Thread.sleep(30000);
            }
        }


    /*        Total number of schedules for the carrier */
    public int totalSchedules()
        {

        return listSize(SchedulesPage.searchResultsarea, SchedulesPage.flightResults);

        }

    /*  Verify if the total number of schedules copied is the same */
    public void verifyCopiedSchedules(String copyType, String fromCarrier, String fromVersion, String toCarrier, String toVersion) throws InterruptedException
        {
        switch (copyType)
            {
            case "all":
                verifyAllCopiedSchedules(fromCarrier, fromVersion, toCarrier, toVersion);
                break;
            default:
                System.out.println("None of the copy range is provided");
            }
        }


    /* Verify copied schedules by all */
    public void verifyAllCopiedSchedules(String carrier1, String version1, String carrier2, String version2) throws InterruptedException
        {

        selectACarrier(carrier1, version1);
        int fromSchedules = totalSchedules();
        System.out.println("Total number of schedules in the carrier ** " + carrier1 + "with version " + version1 + " are -- " + fromSchedules);

        selectACarrier(carrier2, version2);
        int toSchedules = totalSchedules();
        System.out.println("Total number of schedules in the carrier ** " + carrier2 + "with version " + version2 + " are -- " + toSchedules);

        Assert.assertEquals(fromSchedules, toSchedules);
        }


    /*        Verify copied schedules by service range */

    public void verifyCopiedServiceByRange(String carrier, String version, String fromRange, String toRange) throws InterruptedException
        {
        selectACarrier(carrier, version);
        waitForElementToBePresent(SchedulesPage.flightResults);
        List<WebElement> flights = findElement(SchedulesPage.searchResultsarea).findElements(SchedulesPage.flightResults);
        for (WebElement flight : flights)
            {
            int i = Integer.parseInt(flight.getText().replaceAll(carrier, "").trim());
            System.out.println("The integer after parsing is ******************" + i);
            Assert.assertTrue("Range is not within the desired range", i >= Integer.parseInt(fromRange) && i <= Integer.parseInt(toRange));
            }

        }

    /*        Verify copied schedules by service type */
    public void verifyCopiedServiceByType(String carrier, String version, String type) throws InterruptedException
        {
        selectACarrier(carrier, version);
        ip.verifyServiceTypeFieldonEachitenerary(type);


        }

    /* Verify copied service by date*/
    public void verifyCopiedServiceByDate(String carrier, String version, String date1, String date2) throws InterruptedException, ParseException
        {
        selectACarrier(carrier, version);
        ip.verifyFromAndToDatesonItenerary(date1, date2);
        }

    /* Verify copied service by port*/

    public void verifyCopiedServiceByPort(String carrier, String version, String port) throws InterruptedException
        {
        selectACarrier(carrier, version);
        ip.verifyPortFieldsContainCopiedPort(port);


        }

    /*verify ad hoc bulk update to equipment type*/

    public void verifyEquipmentByAdhocBulkUpdate(String carrier, String version, String equipType) throws InterruptedException
        {
        selectACarrier(carrier, version);
        ip.verifyEquipmentField(equipType);
        }


    /*      Enter the from date range */
    public void enterFromDateRange(String date)
        {
        input(fromdateRange, date);

        }

    /*        Enter the TO date range */
    public void enterToDateRange(String date)
        {
        input(toDateRange, date);
        }

    /* Enter  From service number */
    public void enterFromServiceNumber(String i)
        {

        input(fromSerNo, i);
        }

    /* Enter  to service number */
    public void enterToServiceNumber(String i)
        {

        input(toSerNo, i);
        }

    /* Choose service type */
    public void selectServiceType(String type)
        {
        selectByVisibleText(serviceTypeDropdown, type);
        }


    /*   Choose port from dropdown */

    public void selectPort(String port)
        {
        selectByVisibleText(portDropdown, port);
        }

    /*        AD HOC BULK UPDATE */

    public void enterFileName(String fileName)
        {
        input(fileNameField, fileName);

        }
}

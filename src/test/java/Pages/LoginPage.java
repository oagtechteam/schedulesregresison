package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

@SuppressWarnings("All")
public class LoginPage extends BasePage {
    SchedulesPage sp = new SchedulesPage();

    private By signOutButton = By.cssSelector("img[src='images/buttons/signout.gif']");
    private By usernameField = By.name("j_username");
    private By passwordField = By.name("j_password");
    private By signInButton = By.xpath("//input[contains(@src,'images/buttons/signin.gif')]");
    private By terms = By.name("termsCheck");

    /* Navigating to the schedules url */
    public void naviagetToSchedules()
        {
        goTourl(System.getProperty("baseUrl"));
        Assert.assertTrue(getCurrentUrl().startsWith(System.getProperty("baseUrl") ) );
        }

    /*      Check if the user is logged in */
    public boolean isUserLoggedin()
        {

        if (getCurrentUrl().equals(System.getProperty("baseUrl"))) {
            return true;
        }
        return false;
        }

    /* Do a default login */
    public void defaultLogin()
        {


        login(System.getProperty("username"), System.getProperty("password"));

        }

    /* Login to the schedules */
    public void login(String username, String password)
        {
        naviagetToSchedules();
        input(usernameField, username);
        input(passwordField, password);
        tickCheckboxIfUnticked(terms);

        click(signInButton);
        expWaitGForUrltoChange(System.getProperty("baseUrl"));
        Assert.assertEquals(System.getProperty("baseUrl"), getCurrentUrl());

        }

    /* Log in if the user is not logge din already */
    public void loginIfUserisNotLoggedIn()
        {
        if (!isUserLoggedin()) {
            defaultLogin();
        } else {
            System.out.println("User is logged in already");


        }
        }


}

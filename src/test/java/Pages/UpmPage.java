package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

@SuppressWarnings("All")
public class UpmPage extends BasePage {

    private By upmLink = By.linkText("Update Process Monitor");

    private By menuheaderCurrentlyIn = By.id("Overview");
    private By wipheader = By.cssSelector(".navbartxt>font");
    private By checkBoxToClick = By.cssSelector("#scrollArea>#printTable>tbody>tr:nth-child(6)>td:nth-child(9)>.txt1>tbody>tr>td>input");
    private By initiateCommitButton = By.xpath("//img[@title='Commit chosen Carriers']");
    public static By confirmation = By.id("errorsMessages");
    ;
    public static By carrierCodeField = By.xpath("//*[@id='printHide']/td[1]/input");
    private By upmVersionDropdown = By.name("scheduleVersionName");
    public static By noOfItems = By.xpath("//*[@id='printTableBottom']/tbody/tr/td[2]");
    public static By upmFilter = By.xpath("//*[@id='printHide']/td[1]/a/img");


    LeftHandMenuPage lp = new LeftHandMenuPage();

    public void confirmInUpm(String exp)
        {
        Assert.assertTrue("User is not in the " + exp + "menu", getTextOfaElement(menuheaderCurrentlyIn).contains(exp));
        }


    public void confirmOnWipMenu(String exp)
        {
        Assert.assertTrue("User is not in the " + exp + "menu", getTextOfaElement(wipheader).contains(exp));
        }

    /* Initiate commit for a non RSD carrier*/
    public void initiateCommit(String carrier, String version, String process) throws InterruptedException
        {
        if (!process.equalsIgnoreCase("manual"))
            {
            lp.clickOnAMenu("Update Process Monitor");
            }
        lp.clickOnAMenu("Work In Progress");

        waitForElementToBePresent(SchedulesPage.searchResultsarea);

        /*Filter out the carrier*/
        waitForElementToBePresent(By.id("printTable"));
        findElement(By.id("printTable")).findElement(carrierCodeField).sendKeys(carrier);
        selectByVisibleText(upmVersionDropdown, version);
        Thread.sleep(1000);
        click(upmFilter);
        waitFortextTobePresent(noOfItems, "1- 1 of 1");


        List<WebElement> carriersInCommit = findElements(By.cssSelector(".dataRow>#colCarrier"));
        System.out.println(carriersInCommit.size() + "Number of carriers found ");
        List<WebElement> carrierVersion = findElements(By.cssSelector(".dataRow>#colVersion'"));
        for (int i = 0; i < carriersInCommit.size(); i++)
            {
            System.out.println(carriersInCommit.get(i).getText());
            System.out.println(carrierVersion.get(i).getText());

            if ((carriersInCommit.get(i).getText().startsWith(carrier))
                    && (carrierVersion.get(i).getText().equalsIgnoreCase(version)
                    && (isProcessPresent("//*[@id='printTable']/tbody/tr[", (i + 3), "]/td[4]/table/tbody", process))))
                {
                System.out.println("Found the carrier ** " + carriersInCommit.get(i).getText() + " With version **" + carrierVersion.get(i).getText() + " at the index of ***" + i);
                waitForElementToBePresent(By.cssSelector("tr[class=\"dataRow\"][valign=\"top\"]:nth-child(" + (i + 3) + ")>td:nth-child(9)>table>tbody>tr>td>input[name='checkingRequiredInd']"));
                WebElement chkBox = findElement(By.cssSelector("tr[class=\"dataRow\"][valign=\"top\"]:nth-child(" + (i + 3) + ")>td:nth-child(9)>table>tbody>tr>td>input[name='checkingRequiredInd']"));
                if (chkBox.isDisplayed() && (!chkBox.isSelected()))
                    {
                    chkBox.click();
                    Thread.sleep(1000);
                    clickCommit();
                    waitForElementToBePresent(confirmation);
                    waitFortextTobePresent(confirmation, "Commit started for: " + carrier + ".");
                    Assert.assertEquals("Commit started for: " + carrier + ".", getTextOfaElement(confirmation));
                    break;

                    }
                if (!chkBox.isDisplayed())
                    {
                    Assert.fail("There are changes to be completed hence checkbox is not available");
                    } else
                    {
                    Assert.fail("Carrier is not initiated for the Commit");
                    }
                } else
                {
                Assert.fail("Could not find the carrier with the process" + process);
                }


            }

        Thread.sleep(10000);
        }


    /*Initiating commit for a RSD carrie rby verifying the RSD date on hover of the check box*/

    public void initCommitRsdCarrier(String carrier, String version, String process, String Rsd) throws InterruptedException
        {


        if (!process.equalsIgnoreCase("manual"))
            {
            lp.clickOnAMenu("Update Process Monitor");
            }
        lp.clickOnAMenu("Work In Progress");
        waitForElementToBePresent(SchedulesPage.searchResultsarea);
        /*Filter out the carrier*/
        findElement(By.id("printTable")).findElement(carrierCodeField).sendKeys(carrier);
        selectByVisibleText(upmVersionDropdown, version);
        Thread.sleep(1000);
        click(upmFilter);
        waitFortextTobePresent(noOfItems, "1- 1 of 1");

        List<WebElement> carriersInCommit = findElements(By.cssSelector(".dataRow>#colCarrier"));
        System.out.println(carriersInCommit.size() + "Number of carriers found ");
        List<WebElement> carrierVersion = findElements(By.cssSelector(".dataRow>#colVersion'"));
        for (int i = 0; i < carriersInCommit.size(); i++)
            {
            System.out.println(carriersInCommit.get(i).getText());
            System.out.println(carrierVersion.get(i).getText());


            if ((carriersInCommit.get(i).getText().startsWith(carrier))
                    && (carrierVersion.get(i).getText().equalsIgnoreCase(version)
                    && (isProcessPresent("//*[@id='printTable']/tbody/tr[", (i + 3), "]/td[4]/table/tbody", process))))
                {
                System.out.println("Found the carrier ** " + carriersInCommit.get(i).getText() + "With version **" + carrierVersion.get(i).getText() + "at the index of ***" + (i + 3) + "With a Process of" + findElement(By.xpath(".//*[@id='printTable']/tbody/tr[" + (i + 3) + "]/td[4]/table/tbody/tr[1]/td/a")).getText());
                waitForElementToBePresent(By.cssSelector("tr[class=\"dataRow\"][valign=\"top\"]:nth-child(" + (i + 3) + ")>td:nth-child(9)>table>tbody>tr>td>input[name='checkingRequiredInd']"));
                WebElement chkBox = findElement(By.cssSelector("tr[class=\"dataRow\"][valign=\"top\"]:nth-child(" + (i + 3) + ")>td:nth-child(9)>table>tbody>tr>td>input[name='checkingRequiredInd']"));
                System.out.println("     The title of the chk box is **********************    " + chkBox.getAttribute("title").replace("RSD:", "").trim());

                Assert.assertEquals(Rsd, chkBox.getAttribute("title").replace("RSD:", "").trim());
                if (chkBox.isDisplayed() && (!chkBox.isSelected()))
                    {

                    chkBox.click();
                    Thread.sleep(1000);
                    clickCommit();
                    waitForElementToBePresent(confirmation);
                    waitFortextTobePresent(confirmation, "Commit started for: " + carrier + ".");
                    Assert.assertEquals("Commit started for: " + carrier + ".", getTextOfaElement(confirmation));
                    break;

                    }
                if (!chkBox.isDisplayed())
                    {
                    Assert.fail("There are changes to be completed hence checkbox is not available");
                    } else
                    {
                    Assert.fail("Carrier is not initiated for the Commit");
                    }
                } else
                {
                Assert.fail("Could not find the carrier with the process" + process);
                }

            }


        }

    /*Verify if a process is present in the carrier row*/
    public boolean isProcessPresent(String xpath1, int element, String xpath2, String process)
        {
        boolean exists = false;
        String xpath3 = xpath1 + element + xpath2;
        List<WebElement> processes = findElement(By.xpath(xpath3)).findElements(By.tagName("tr"));
        System.out.println("the total number of processes found are ***** " + processes.size());
        for (WebElement eachprocess : processes)
            {
            eachprocess.findElement(By.tagName("a")).getText();
            System.out.println("The text on  process is ***" + eachprocess.getText());
            if (eachprocess.findElement(By.tagName("a")).getText().equalsIgnoreCase(process))
                {
                exists = true;
                break;
                }
            }
        return exists;
        }

    public void clickCommit()
        {

        click(initiateCommitButton);
        }


}

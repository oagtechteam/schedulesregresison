package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


@SuppressWarnings("ALL")
public class SchedulesPage extends BasePage {

    /*  Buttons */
    public static By filterButton = By.xpath("//*[@id='scrollArea']/table/tbody/tr[2]/td[6]/a/img");
    private By goButton = By.xpath("//img[@src='/SchedulesWeb/images/buttons/go.gif']");
    /*    locators */
    private By menuHeader = By.className("MenuHeader");
    private By schedulesLeftHandMenu = By.className("MenuHeader");
    final String TOPBARFRAMENAME = "topbar";
    final String CARRIERSCHEDULESELECTORFRAME = "ApplicationFrameSet";
    public static By footerAtTheEndofSearchResults = By.cssSelector(".WorkAreaFooter>td");
    public static By searchResultsarea = By.id("scrollArea");
    public static By flightResults = By.cssSelector(".txt>tbody>tr>td>div>a");
    private By rsdLevel = By.name("releaseDate");
    private By currentMenu = By.cssSelector(".WorkTitle>td>strong");
    public static String carrierChoosen;
    private By newLevelRsdInputField = By.name("rsd");
    private By newRsdDateField = By.xpath("//*[@id='scrollArea']/table/tbody/tr[3]/td[5]");


    /*    inputs */
    public static By carrierSelectorField = By.name("code");

    /* Dropdowns */
    public static By scheduleVersionDropdown = By.name("scheduleVersion");
    private By rsdDateDropdown = By.cssSelector(".WorkTitle>td>select");
//            By.name("releaseDate");

    TopBarPage tp = new TopBarPage();

    /* Select a carrier by name */
    public void carrierName(String carrier)
        {
        input(carrierSelectorField, carrier);

        }

    /* Select a schedule version form the dropdown */
    public void selectScheduleVersion(String version) throws InterruptedException
        {

        selectByVisibleText(scheduleVersionDropdown, version);
        }

    public void selectRsdlevel(String rsd)
        {

        selectByVisibleText(rsdLevel, rsd);
        }

    /*        Verify if footer is present  */
    public void isFooterPresent()
        {
        waitForElementExplicitly(footerAtTheEndofSearchResults);
        isElementDisplayed(footerAtTheEndofSearchResults);
        }

    /*        Click filter */
    public void clickFilterButton()
        {
        click(filterButton);
        }

    /*        Choose a carrier base don schedule version */
    public void chooseACarrierWithversion(String carrier, String version, String rsd) throws InterruptedException
        {
        carrierName(carrier);
        selectScheduleVersion(version);
        selectRsdlevel(rsd);
        clickFilterButton();
        isFooterPresent();
        Thread.sleep(3000);
        clickIfRsdCarrier();
        waitForElementToBePresent(searchResultsarea);
        verifyValuesInAList(searchResultsarea, flightResults, carrier);
        this.carrierChoosen = carrier;
        System.out.println(carrierChoosen + "-- IS THE CARRIER CHOSEN");


        }

        /* click the carrier on initial level for a rsd carrier */
    public void clickIfRsdCarrier() throws InterruptedException
        {
        if (getTextOfaElement(currentMenu).equalsIgnoreCase("Carrier Schedules Selection")) {
            click(By.cssSelector(".txt1>a"));
            Thread.sleep(3000);
        }
        }

    /*        Choose add new level from the dropdown */
    public void chooseAddNewLevelFromDropdown()
        {
        waitForElementToBePresent(rsdDateDropdown);
        selectByVisibleText(rsdDateDropdown, "Add New Level");
        }

    /*        Add a new level from route summary */
    public void addNewLevel() throws InterruptedException
        {

        String parentWindow = parentWindowHandle();
        chooseAddNewLevelFromDropdown();
        Thread.sleep(2000);
        switchWindow();
        input(newLevelRsdInputField, pickTodaysDate().toUpperCase());
        click(goButton);
//        WebConnector.currentDriver().close();

        }


      /*  Verify a newly added RSD level */

    public void verifyAddedRsdLevel(String carrier, String version, String rsd) throws InterruptedException
        {
        tp.navigateToschedules();
        chooseACarrierWithversion(carrier, version, rsd);
        Assert.assertEquals(pickTodaysDate().toUpperCase(), findElement(newRsdDateField).getText());
        }


    /*Choose first option from results*/
    public void chooseFirstFlightFromResults(int index) throws InterruptedException
        {

    Thread.sleep(2000);

    WebElement fligtsArea = findElement(searchResultsarea);
    List<WebElement> flights = fligtsArea.findElements(flightResults);
    flights.get(index-1).click();

    }


        }



@All @reports @ear
Feature: Reports
  As a schedules user
  I should be able to access various reports
  So I can verify certain reports for carriers

  Background:
    Given Iam logged in
    And Iam on "schedules" menu
    And I click on "Reports" menu

  @listing
  Scenario: Listing Report
    When I click on "Listing Report" menu
    And I enter "email" as "automation@oag.com"
    And I enter designator as "ZZ"
    And I choose version as "Test"
    When I click the "submit" button
    Then I should see the report confirmation "Report generation has been started. You will be emailed the result."

  @reportByServiceType
  Scenario: Service Type report
    When I click on "Listing Report" menu
    And I enter "email" as "automation@oag.com"
    And I enter designator as "ZZ"
    And I choose version as "Test"
    And I choose service type as "J"
    When I click the "submit" button
    Then I should see the report confirmation "Report generation has been started. You will be emailed the result."

  @reportByServiceNo
  Scenario: Service Type report
    When I click on "Listing Report" menu
    And I enter "email" as "automation@oag.com"
    And I enter designator as "ZZ"
    And I choose version as "Test"
    And I enter service from as "1" and service to as "5"
    When I click the "submit" button
    Then I should see the report confirmation "Report generation has been started. You will be emailed the result."


  @reportByDeletedCarrierCode
  Scenario: Deleted carrier report by carrier code
    When I click on "Deleted Carrier Report" menu
    And I enter "carrier code" as "ZZ"
    And I choose "carrier version" as "Test"
    When I click the "submit" button
    Then I should see the report confirmation "Report generation successful. Please check the report in /oagshared/" "env" "/schedules/report/deletedReports/"

  @carrierNameandAssociatedNames
  Scenario: Deleted carrier report by carrier name and associated names
    When I click on "Deleted Carrier Report" menu
    And I enter "carrier code" as "ZZ"
    And I tick the "search for all names" checkbox
    When I click the "submit" button
    Then I should see the report confirmation "Report generation successful. Please check the report in /oagshared/" "env" "/schedules/report/deletedReports/"

  @reportByCarrierName
  Scenario: Deleted carrier report by carrier name and associated names
    When I click on "Deleted Carrier Report" menu
    And I choose "name" as "Test Carrier For OAG links users"
    When I click the "submit" button
    Then I should see the report confirmation "Report generation successful. Please check the report in /oagshared/" "env" "/schedules/report/deletedReports/"

  @ssmINCReport
  Scenario: SSM INC report
    When I click on "SSM ASM NAC Report" menu
    When I click the "submit" button
    Then I should see the report confirmation "Report has been submitted. Please check the report in /oagshared/" "env" "/schedules/report/ssm/incmessages/"

  @ssmNACReport
  Scenario: SSM NAC report
    When I click on "SSM ASM NAC Report" menu
    And I choose "Nac Inc" as "NAC"
    When I click the "submit" button
    Then I should see the report confirmation "Report has been submitted. Please check the report in /oagshared/" "env" "/schedules/report/ssm/nacmessages/"




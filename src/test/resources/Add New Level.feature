Feature: Add a new level RSD
  As a Schedules user
  I should be able to add a new RSD level on a RSD carrier

  @newlevel
  Scenario Outline: Add a new level
    Given Iam logged in
    And Iam on "Schedules" menu
    When I search for "<carrier>" with "<version>" with "Current Level Only" RSD
    Then I should be able to add a new RSD level for "<carrier>" with "<version>" with "Current Level Only" RSD
#    And I should be able to initiate commit for "<string>" with "<string>" with "<string>" verifying"<string>"

    Examples:
      | carrier | version |
      | 5T      | Schedules    |



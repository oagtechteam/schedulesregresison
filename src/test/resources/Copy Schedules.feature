@All @ear @copy
Feature: Copy Schedules

  As a Schedules user
  I should be able to copy services

  Background:
    Given Iam logged in
    And Iam on "Schedules" menu
    When I click on "Bulk Updates" menu
    And I click on "Copy Schedules" menu


  @serviceNo
  Scenario Outline: Copy schedules by service range
    When I enter the "from carrier" as "<From Carrier>" with "<From version>"
    And I enter the "to carrier" as "<To carrier>" with "<To version>"
    And I enter "from service number" as "<range from>"
    And I enter "to service number" as "<range to>"
    When I click the "submit" button
    Then the confirmation should contain "A process for Copy Schedules has been started with Work Id"
#    Then  "service range" schedules should be copied "<From Carrier>" with "<From version>" version to the "<To carrier>" with "<To version>" version
    Then  "service range" schedules should be copied  to the "<To carrier>" with "<To version>" version between "<range from>" and "<range to>"
    Then I should be able to initiate commit for "<To carrier>" with "<To version>" and "CopySchedules"
    Examples:
      | From Carrier | From version | To carrier | To version | range from | range to |
      | ZZ           | Training 2   | ZZ         | Test       | 1          | 5        |

  @serviceType
  Scenario Outline: Copy schedules by service type
    When I enter the "from carrier" as "<From Carrier>" with "<From version>"
    And I enter the "to carrier" as "<To carrier>" with "<To version>"
    And I choose "service type" as "<service type>"
    When I click the "submit" button
    Then the confirmation should contain "A process for Copy Schedules has been started with Work Id"
#    Then  "service type" schedules should be copied "<From Carrier>" with "<From version>" version to the "<To carrier>" with "<To version>" version
    Then schedules should be copied  to the "<To carrier>" with "<To version>" version with service type "<service type>"
    Then I should be able to initiate commit for "<To carrier>" with "<To version>" and "CopySchedules"
    Examples:
      | From Carrier | From version | To carrier | To version | service type |
      | ZZ           | Training 2   | ZZ         | Test       | J            |

  @copyCarrier
  Scenario Outline: Copy schedules carrier wise
    When I enter the "from carrier" as "<From Carrier>" with "<From version>"
    And I enter the "to carrier" as "<To carrier>" with "<To version>"
    When I click the "submit" button
    Then the confirmation should contain "A process for Copy Schedules has been started with Work Id"
    Then  "all" schedules should be copied "<From Carrier>" with "<From version>" version to the "<To carrier>" with "<To version>" version
    Then I should be able to initiate commit for "<To carrier>" with "<To version>" and "CopySchedules"

    Examples:
      | From Carrier | From version | To carrier | To version |
      | ZZ           | Training 2   | ZZ         | Test       |


  @dateRange
  Scenario Outline: Copy schedules with date range.
    When I enter the "from carrier" as "<From Carrier>" with "<From version>"
    And I enter the "to carrier" as "<To carrier>" with "<To version>"
    And I enter "from date" as "<From date>"
    And I enter "to date" as "<To date>"
    When I click the "submit" button
    Then the confirmation should contain "A process for Copy Schedules has been started with Work Id"
#    Then  "date range" schedules should be copied "<From Carrier>" with "<From version>" version to the "<To carrier>" with "<To version>" version
    Then  "date range" schedules should be copied  to the "<To carrier>" with "<To version>" version between "<From date>" and "<To date>"
    Then I should be able to initiate commit for "<To carrier>" with "<To version>" and "CopySchedules"

    Examples:
      | From Carrier | From version | To carrier | To version | From date | To date |
      | ZZ           | Training 2   | ZZ         | Test       | 15May18   | 15JUN18 |

  @byPort
  Scenario Outline: Copy schedules by port.
    When I enter the "from carrier" as "<From Carrier>" with "<From version>"
    And I enter the "to carrier" as "<To carrier>" with "<To version>"
    And I choose "port" as "<port>"
    When I click the "submit" button
    Then the confirmation should contain "A process for Copy Schedules has been started with Work Id"
    Then schedules should be copied  to the "<To carrier>" with "<To version>" version with port "<port>"
    Then I should be able to initiate commit for "<To carrier>" with "<To version>" and "CopySchedules"
    Examples:
      | From Carrier | From version | To carrier | To version | port |
      | ZZ           | Training 2   | ZZ         | Test       | JER  |

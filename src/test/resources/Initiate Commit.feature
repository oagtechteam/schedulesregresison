@All @ear
Feature: Initiate manual commit
  As a schedules user
  I should be able to initiate a commit manually for a carrier.


  @initiateCommit
  Scenario Outline:Initiate the commit
    Given Iam logged in
    And Iam on "schedules" menu
    When I search for "<carrier>" with "<version>" with "Current Level Only" RSD
    And I  edit and  save each itenerary
    Then I should be able to initiate commit for "<carrier>" with "<version>" and "<process>"

    Examples:
      | carrier | version    | process |
      | ZZ      | Training 2 | manual  |


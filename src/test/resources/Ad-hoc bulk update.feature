@All @ear
Feature: Ad hoc bulk update
  As a schedules user
  I should be able to update a carrier via bulk updates

  @bulk
  Scenario Outline: Ad hoc bulk update
    Given Iam logged in
    And Iam on "Schedules" menu
    When I click on "Bulk Updates" menu
    And I click on "Ad-hoc bulk update" menu
    When I enter designator as "<designator>"
    And I choose "version" as "<version>"
    And I enter "file name" as "<filename>"
    And I click the "submit" button
    Then I should see the status "Adhoc bulk update is successful"
    And the carrier "<designator>" with version "<version>" should be updated with equipment "<equipment type>"
    Then I should be able to initiate commit for "<designator>" with "<version>" and "BulkUpdate"

    Examples:
      | designator | version    | filename       | equipment type |
      | SD         | Training 2 | autotests1.txt | 747            |

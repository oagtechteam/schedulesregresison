@mandatory
Feature: Mandatory Fields
  As a Schedules user
  I should be able to verify the mandatory fields
  So that the error messages are picked up correctly


#  Background:

  @dates
  Scenario: Auto fill start and end date
    Given Iam logged in
    And Iam on "Schedules" menu
    When I search for "ZZ" with "Training 1" with "Current Level Only" RSD
    And I choose 1 st flight and  itenerary
    And I click the "edit" button
    When I delete the eff from and eff to date
    When I click the "save" button
    Then the date should be added automatically

  @minDaysOfOp @dop
  Scenario: Error message verification on blank DaysOfOP
    When I delete the "operational days" field
    When I click the "save" button
#    Then  the error message "Please review the item(s) marked in red before saving" should be shown
    And  the "minimum days" error message "Days can not be less than 1 characters." should be displayed

  @maxDaysOfOp @dop
  Scenario: Error message verification on  DOP more than 7
    When I add more then seven characters in operational days field
    And I click the "save" button
    Then the "maximum days" error message "Days can not be greater than 7 characters." should be displayed

  @depPort
  Scenario: Blank dep port
    When I delete the "dep port" field
    And I click the "save" button
    Then the "dep port" error message "Port Code is required." should be displayed

  @arrPort

  Scenario: Blank arr port
    When I delete the "arr port" field
    And I click the "save" button
    Then the "arr port" error message "Port Code is required." should be displayed

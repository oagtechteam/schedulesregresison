@All @ear @removeSchedules
Feature: Remove Schedules from a carrier
  As a Schedules user
  I should be able to remove the schedules associated with a carrier.


  Background:
    Given Iam logged in
    And Iam on "Schedules" menu
    When I click on "Bulk Updates" menu
    And I click on "Remove Schedules" menu


  @remove
  Scenario Outline: Remove Schedules

    When I add designator as "<carrier>"
    And I choose "version" as "<version>"
    When I click the "submit" button
    Then I should see the status "Remove Schedules is successful"
    And I should be able to remove schedules for "<carrier>" with "<version>"
    Then I should be able to initiate commit for "<carrier>" with "<version>" and "DeleteSchedules"

    Examples:
      | carrier | version |
      | ZZ      | Test    |


  @removeRsdSchedules
  Scenario Outline: Remove schedules for RSD carrier

    When I add designator as "<carrier>"
    And I choose "version" as "<version>"
    And I choose "rsddate" as "<rsd date>"
    When I click the "submit" button
    Then I should see the status "Remove Schedules is successful"
    And I should be able to remove schedules for "<carrier>" with "<version>"
    Then I should be able to initiate commit for "<carrier>" with "<version>" with "DeleteSchedules" verifying"<rsd date>"

    Examples:
      | carrier | version | rsd date |
      | DT      | Test    | 21FEB17  |
    